from django.db import models
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.contrib.auth.models import User
from datetime import date


# Create your models here.
class Restaurant(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class RestaurantMenu(models.Model):
    name = models.CharField(max_length=100)
    menu_image = models.FileField()
    restaurant_obj = models.ForeignKey(
        'Restaurant',
        on_delete=models.CASCADE,
    )
    day_choices = (
        ('monday', 'Monday'),
        ('tuesday', 'Tuesday'),
        ('wednesday', 'Wednesday'),
        ('thursday', 'Thursday'),
        ('friday', 'Friday'),
        ('saturday', 'Saturday'),
        ('sunday', 'Sunday'),
    )
    weekdays = (
        (0, 'Monday'),
        (1, 'Tuesday'),
        (2, 'Wednesday'),
        (3, 'Thursday'),
        (4, 'Friday'),
        (5, 'Saturday'),
        (6, 'Sunday'),
    )

    day_choice = models.CharField(
        max_length=10,
        choices=day_choices,
        default='tuesday',
    )

    weekday_select = models.IntegerField(
        choices=weekdays,
        default=date.today().weekday(),
        null=True,
        blank=True,
    )
    def __str__(self):
        return self.name

class RestaurantPoll(models.Model):

    created = models.DateField(editable=False)
    day_choices = (
        ('monday', 'Monday'),
        ('tuesday', 'Tuesday'),
        ('wednesday', 'Wednesday'),
        ('thursday', 'Thursday'),
        ('friday', 'Friday'),
        ('saturday', 'Saturday'),
        ('sunday', 'Sunday'),
    )

    weekdays = (
        (0, 'Monday'),
        (1, 'Tuesday'),
        (2, 'Wednesday'),
        (3, 'Thursday'),
        (4, 'Friday'),
        (5, 'Saturday'),
        (6, 'Sunday'),
    )

    day_choice = models.CharField(
        max_length=10,
        choices=day_choices,
        default='tuesday',
    )

    weekday_select = models.IntegerField(
        choices=weekdays,
        default=date.today().weekday(),
        null=True,
        blank=True,
    )

    restaurant_menu_obj = models.ForeignKey(
        'RestaurantMenu',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        editable=False,
    )

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created = timezone.now()
        return super(RestaurantPoll, self).save(*args, **kwargs)

    def __str__(self):
        return "%s Poll - %s" % (self.get_day_choice_display(), self.created)

    # def clean_validate_menu_day(self):
    #     """Method to ensure that correct day menu selected."""
    #     if self.restaurant_menu_obj.day_choice != self.day_choice:
    #         raise ValidationError('You must select %s menu' % self.day_choice)

class RestaurantChoice(models.Model):

    restaurant_poll_obj = models.ForeignKey(
        'RestaurantPoll',
        on_delete=models.CASCADE,
    )
    restaurant_menu_obj = models.ForeignKey(
        'RestaurantMenu',
        on_delete=models.CASCADE,
    )
    users = models.ManyToManyField(User)
    votes_count = models.IntegerField(default=0)