from django.contrib import admin

# Register your models here.
from .models import Restaurant
from .models import RestaurantMenu
from .models import RestaurantPoll
from .models import RestaurantChoice

admin.site.register(Restaurant)
admin.site.register(RestaurantMenu)
admin.site.register(RestaurantPoll)
admin.site.register(RestaurantChoice)