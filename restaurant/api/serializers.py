from django.contrib.auth.models import User, Group
from rest_framework import serializers
from restaurant.models import Restaurant, RestaurantMenu, RestaurantPoll, RestaurantChoice


class RestaurantSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Restaurant
        fields = ('url','id','name',)

class RestaurantMenuSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = RestaurantMenu
        fields = ('url', 'id', 'name', 'menu_image', 'restaurant_obj', 'day_choice')

class RestaurantPollSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = RestaurantPoll
        fields = ('url', 'id', 'weekday_select', 'restaurant_menu_obj', 'day_choice')

class RestaurantChoiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RestaurantChoice
        fields = (
            'url',
            'id',
            'restaurant_menu_obj',
            'restaurant_poll_obj',
            'users',
            'votes_count',
        )


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password')

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        user.groups.add(Group.objects.get(name='Employee'))
        return user
