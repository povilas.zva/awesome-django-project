from django.conf.urls import url
from django.urls import path, include
from .views import RestaurantView, RestaurantMenuView, \
    RestaurantPollView, RestaurantChoiceView, UserView, restaurant_vote
from rest_framework import routers

router = routers.DefaultRouter()
router.register('restaurants', RestaurantView)
router.register('menu', RestaurantMenuView)
router.register('poll', RestaurantPollView)
router.register('voting', RestaurantChoiceView)
router.register('users',UserView)
urlpatterns = [
    path('', include(router.urls)),
    url('vote/', restaurant_vote)
]