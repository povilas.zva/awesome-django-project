from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from rest_framework import viewsets
from .serializers import RestaurantSerializer, RestaurantMenuSerializer, \
    RestaurantPollSerializer, RestaurantChoiceSerializer, UserSerializer
from restaurant.models import Restaurant, RestaurantMenu, \
    RestaurantPoll, RestaurantChoice
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required, user_passes_test
from datetime import date



class RestaurantView(viewsets.ModelViewSet):

    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer


class RestaurantMenuView(viewsets.ModelViewSet):

    queryset = RestaurantMenu.objects.all()
    serializer_class = RestaurantMenuSerializer

    def get_queryset(self):
        day = self.request.query_params.get('day')
        if day:
            queryset = RestaurantMenu.objects.filter(
                day_choice=day)
        else:
            queryset = RestaurantMenu.objects.all()
        return queryset


class RestaurantPollView(viewsets.ModelViewSet):

    queryset = RestaurantPoll.objects.all()
    serializer_class = RestaurantPollSerializer

    def get_queryset(self):
        today = self.request.query_params.get('today')
        if today:
            queryset = RestaurantPoll.objects.filter(
                created=date.today())
        else:
            queryset = RestaurantPoll.objects.all()
        return queryset


class RestaurantChoiceView(viewsets.ReadOnlyModelViewSet):

    queryset = RestaurantChoice.objects.all()
    serializer_class = RestaurantChoiceSerializer


class UserView(viewsets.ModelViewSet):

    queryset = User.objects.all()
    serializer_class = UserSerializer


def is_employee(user):
    return user.groups.filter(name='Employee').exists()


def restaurant_vote(request):
    if request.user.is_authenticated and is_employee(request.user):
        menu = request.GET.get('menu')
        poll = request.GET.get('poll')
        poll_obj = RestaurantPoll.objects.get(id=poll)
        menu_obj = RestaurantMenu.objects.get(id=menu)
        if poll_obj.weekday_select == menu_obj:
            vote, created = RestaurantChoice.objects.get_or_create(
                restaurant_menu_obj = menu_obj, restaurant_poll_obj = poll_obj,
                defaults={
                    "restaurant_poll_obj_id": poll,
                    "restaurant_menu_obj_id": menu,
                })
            if created:
                created.save()
                created.users.add(request.user)
                created.votes_count += 1
            elif not vote.users.filter(id=request.user.id).exists():
                vote.votes_count += 1
                vote.save()
                vote.users.add(request.user)
            return HttpResponse("Voted")
        else:
            raise ValidationError(
                "You must select %s menu"
                % poll_obj.get_weekday_select_display()
            )
    return HttpResponseRedirect('/api-authlogin')


def restaurant_unvote(request):
    if request.user.is_authenticated:
        vote = RestaurantChoice.objects.get(id=request.GET.get('vote'))
        if vote.exists() and vote.users.filter(id=request.user.id).exists():
            vote.users.remove(request.user)
            vote.votes_count -= 1
        return HttpResponse("Unvoted")